<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');

Route::get('/tampilan_navigasi_auto', 'ControllerNavigasiAuto@index');
Route::get('/powermonitoring', 'powerMonitoringController@index');
Route::get('/deviceregisterGetValue', 'deviceregisterController@deviceregisterGetValue');

/***********device trending********/
Route::get('/device_trending', 'TrendingController@index');
Route::get('/device_trendingdate/{waktumin}/{waktumax}/{jammin}/{jammax}','TrendingController@device_trendingByDate');
/********end device trending *************/
/*****device consumption ***/
Route::get('/consumption', 'DeviceConsumption@index');
Route::get('/device_consumptiondate/{waktu}', 'DeviceConsumption@getValueByMonth');
Route::get('/device_consumptiondate2/{waktu}', 'DeviceConsumption@getValueByBydate');
Route::get('/device_consumptiondate3/{waktu}', 'DeviceConsumption@getValueByHour');
Route::get('/device_consumptionmenit/{waktu}', 'DeviceConsumption@getValueByMinute');
/** end device consumption*/
/** start billing */
Route::get('/billing', 'BillingController@index');
Route::get('/cetakbilling', 'BillingController@cetakbilling');
	//** end billing**/
Route::resource('deviceregister', 'deviceregisterController');
Route::resource('user', 'UserController');
