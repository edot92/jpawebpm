<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\deviceregister;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class deviceregisterController extends Controller
{

 public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $deviceregister = deviceregister::paginate(10);

        return view('pages.deviceregister.index', compact('deviceregister'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('pages.deviceregister.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['register_label_' => 'required', 'register_rtu_' => 'required', 'register_tcp_' => 'required', 'display_option_' => 'required', 'register_multiply_' => 'required', ]);

        deviceregister::create($request->all());

        Session::flash('flash_message', 'deviceregister added!');

        return redirect('deviceregister');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $deviceregister = deviceregister::findOrFail($id);

        return view('pages.deviceregister.show', compact('deviceregister'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $deviceregister = deviceregister::findOrFail($id);

        return view('pages.deviceregister.edit', compact('deviceregister'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['register_label_' => 'required', 'register_rtu_' => 'required', 'register_tcp_' => 'required', 'display_option_' => 'required', 'register_multiply_' => 'required', ]);

        $deviceregister = deviceregister::findOrFail($id);
        $deviceregister->update($request->all());

        Session::flash('flash_message', 'deviceregister updated!');

        return redirect('deviceregister');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        deviceregister::destroy($id);

        Session::flash('flash_message', 'deviceregister deleted!');

        return redirect('deviceregister');
    }


    public function importExport()
    {
        return view('importremis');
    }
    public function downloadExcel($type)
    {
        $data = Remi::get()->toArray();
        return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
    public function importExcel(Request $request)
    {
        if($request->hasFile('import_file')){
    
            
            $path = $request->file('import_file');
        
       
            $data = Excel::load($path, function($reader) {
            })->get();
    
            if(!empty($data) && $data->count()){
            
                foreach ($data as $key => $value) {
                    $insert[] = ['id' => $value->id, 'nama_kantor' => $value->namakantor
                    , 'uang_tunai' => $value->uang_tunai, 'uang_non_tunai' => $value->uang_non_tunai, 'jumlah_uang_tunai' => $value->jumlah_uang_tunai, 'jumlah_uang_non_tunai' => $value->jumlah_uang_non_tunai];
                }
                if(!empty($insert)){
                    DB::table('Selisih_remis')->insert($insert);
                    $deviceregister = deviceregister::paginate(10);

                    // return view('pages.deviceregister.index', compact('deviceregister'));
                    return redirect()->route('pages.deviceregister.index');
                    //dd('Insert Record successfully.');
                }
            }
        }
        return back();
    }
    public  function deviceregisterGetValue()
    {
        $deviceregister = deviceregister::all();
        return $deviceregister;
    }
}
