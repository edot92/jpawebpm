<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deviceregister extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'device_registers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['register_label_', 'register_rtu_', 'register_tcp_', 'display_option_', 'register_multiply_','register_value_'];
}
//php artisan crud:generate DeviceRegister --fields= "register_label_#string#required, register_rtu_#string#required, register_tcp_#string#required, display_option_#string#required, register_multiply_#string#required" --route=yes --pk=id --view-path="page"
//php artisan crud:view deviceregister --fields="register_label_#string#required, register_rtu_#string#required, register_tcp_#string#required, display_option_#string#required, register_multiply_#string#required" --view-path="page"