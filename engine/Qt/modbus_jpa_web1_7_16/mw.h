#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include "3rdparty/libmodbus/src/modbus-rtu.h"
#include "3rdparty/libmodbus/src/modbus-tcp.h"
#include "3rdparty/libmodbus/src/modbus.h"

#include <errno.h>
#include <QDebug>
#include <QTimer>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QMessageBox>
#include "sql/querythread.h"
#include "sql/db.h"

#include <QDebug>
#include <QDateTime>
#include <QList>
#include <QSqlRecord>
#include "sql/sqlrecmodel.h"
#include <QSqlError>
#include <modbusthread.h>
namespace Ui {
class MW;
}

class MW : public QMainWindow
{
    Q_OBJECT
public:
    explicit MW(QWidget *parent = 0);
    ~MW();
private slots:
    void on_pushButton_clicked();
    void timUpdateModbus();
    void slotGo();
    void slotResults(const QString &queryId, const QList<QSqlRecord> &records, const QString &resultId);
    void slotResultsErrorString(QString,QString,QString);
    void Signalmb_readIntHoldingResultsFromModbusThread(QString,QStringList d);
     void Signalmb_readIntHoldingResultsFromModbusThreadWithDisplayOption(QString,QStringList d);
    void UpdateJam();
    void on_pbTestDatabase_clicked();

signals:
    void exec(const QString &);
    void  Signalsmb_readIntHoldingToModbusThread(int idSlave,QString listAddr,QStringList d);

    void  Signalsmb_readIntHoldingToModbusThreadWithDisplayOption(int idSlave,QString listAddr,QString displayOption,QStringList d);

private:
    Ui::MW *ui;
    QueryThread* m_querythread;
    SqlRecModel *m_model;
    modbusthread *modbusthread_;
    void dispatch(const QString &queryId, const QString &query);
    void initMysql();
    void db_selectAddrModbus();
    void db_selectAddrModbus2(QString j);

void db_simpan_valueByid(QString val);
void db_simpan_valuePerMenit(QString val);
    QTimer *timModbus;
    modbus_t *m_serialModbus;



    QString mb_readFloatHolding(int idSlave,int from , int to)
    {
        modbus_t *ctx;
        uint16_t tab_reg[2] = {0,0};
        float avgVLL = -1;;
        int res = 0;
        int rc;
        int i;
        struct timeval response_timeout;
        uint32_t tv_sec = 0;
        uint32_t tv_usec = 0;
        response_timeout.tv_sec = 5;
        response_timeout.tv_usec = 0;
        setbuf(stdout, NULL);

        ctx = modbus_new_rtu("COM4", 9600, 'N', 8, 1);
        if (NULL == ctx)
        {
            printf("Unable to create libmodbus context\n");
            res = 1;
        }
        else
        {
            printf("created libmodbus context\n");
            modbus_set_debug(ctx, FALSE);
            //modbus_set_error_recovery(ctx, MODBUS_ERROR_RECOVERY_LINK |MODBUS_ERROR_RECOVERY_PROTOCOL);
            rc = modbus_set_slave(ctx, 1);
            printf("modbus_set_slave return: %d\n",rc);
            if (rc != 0)
            {
                printf("modbus_set_slave: %s \n",modbus_strerror(errno));
            }

            /* Commented - Giving 'Bad File Descriptor' issue
               rc = modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS485);
               printf("modbus_rtu_set_serial_mode: %d \n",rc);

               if (rc != 0)
               {
                               printf("modbus_rtu_set_serial_mode: %s \n",modbus_strerror(errno));
               }
               */


            /* This code is for version 3.1.2*/
            modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
            printf("Default response timeout:%d sec %d usec \n",tv_sec,tv_usec );

            tv_sec = 5;
            tv_usec = 0;

            modbus_set_response_timeout(ctx, tv_sec,tv_usec);
            modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
            printf("Set response timeout:%d sec %d usec \n",tv_sec,tv_usec );


            rc = modbus_connect(ctx);
            printf("modbus_connect: %d \n",rc);

            if (rc == -1) {
                printf("Connection failed: %s\n", modbus_strerror(errno));
                res = 1;

                return "port salah";
            }

            rc = modbus_read_registers(ctx, 0, 2, tab_reg);
            printf("modbus_read_registers: %d \n",rc);

            if (rc == -1) {
                printf("Read registers failed:  %s\n", modbus_strerror(errno));
                res = 1;
                return "timeout device";
            }

            for (i=0; i < 2; i++) {
                printf("reg[%d]=%d (0x%X)\n", i, tab_reg[i], tab_reg[i]);
            }

            avgVLL = modbus_get_float_dcba(tab_reg);

            // printf("Average Line to Line Voltage = %f\n", avgVLL);

            modbus_close(ctx);
            modbus_free(ctx);
            return QString::number(avgVLL,'f',4);
        }
        return "false";

    }

    void read03(int idSlave,int from , int to){
        modbus_t *ctx;
        uint16_t tab_reg[64];
        int rc;
        int i;

        ctx =  modbus_new_rtu("COM4", 2400, 'N',8,1);
        modbus_flush(ctx);
        if (modbus_connect(ctx) == -1) {
            qDebug()<<"belum konek";
            fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
            modbus_free(ctx);

        }
        modbus_set_debug(ctx, FALSE);
        modbus_set_slave(ctx,idSlave);

        rc = modbus_read_registers(ctx, 0, 10, tab_reg);
        if (rc == -1) {
            fprintf(stderr, "%s\n", modbus_strerror(errno));

            qDebug() <<  "error baca" ;
            qDebug() <<  modbus_strerror(errno);
            modbus_close(ctx);

        }else
        {

            for (i=0; i < rc; i++)
            {
                //  printf("reg[%d]=%d (0x%X)\n", i, tab_reg[i], tab_reg[i]);
                qDebug()<< tab_reg[i];
            }
            modbus_close(ctx);
            modbus_free(ctx);
        }



    }

    QString mb_readIntHolding(int idSlave,QString listAddr)
    {
        modbus_t *ctx;
        uint16_t tab_reg[2] = {0,0};
        float avgVLL = -1;;
        int res = 0;
        int rc;
        int i;
        struct timeval response_timeout;
        uint32_t tv_sec = 0;
        uint32_t tv_usec = 0;
        response_timeout.tv_sec = 5;// timeoutnya
        response_timeout.tv_usec = 0;
        setbuf(stdout, NULL);
qDebug()<<"connect prepare";
        ctx = modbus_new_rtu("COM4", 9600, 'N', 8, 1);
        if (NULL == ctx)
        {
            qDebug()<<("Unable to create libmodbus context\n");
            res = 1;
        }
        else
        {
            printf("created libmodbus context\n");
            modbus_set_debug(ctx, FALSE);
            //modbus_set_error_recovery(ctx, MODBUS_ERROR_RECOVERY_LINK |MODBUS_ERROR_RECOVERY_PROTOCOL);
            rc = modbus_set_slave(ctx, idSlave);
            printf("modbus_set_slave return: %d\n",rc);
            if (rc != 0)
            {
                printf("modbus_set_slave: %s \n",modbus_strerror(errno));
            }


            /* This code is for version 3.1.2*/
            modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
            printf("Default response timeout:%d sec %d usec \n",tv_sec,tv_usec );

            tv_sec = 5;// timeoutnya
            tv_usec = 0;

            modbus_set_response_timeout(ctx, tv_sec,tv_usec);
            modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
            printf("Set response timeout:%d sec %d usec \n",tv_sec,tv_usec );


            rc = modbus_connect(ctx);
            printf("modbus_connect: %d \n",rc);

            if (rc == -1) {
                printf("Connection failed: %s\n", modbus_strerror(errno));
                res = 1;
                modbus_close(ctx);
                modbus_free(ctx);
                return "port salah";
            }else{
                QStringList listAddr_=listAddr.split(",");

                // AMBIL LIST ALAMAT dan data satu2
               QString result_="";
//                for(int x= 0 ;x < listAddr_.count();x++){
//                    modbus_set_response_timeout(ctx, tv_sec,tv_usec);
//                    modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
//                    int tempAlamat=QString(listAddr_.at(x)).toInt();
//                    rc = modbus_read_registers(ctx,tempAlamat, 1, tab_reg);
//                    printf("modbus_read_registers: %i cnt= %i \n",tempAlamat,x);

//                    if (rc == -1) {
//                        printf("Read registers failed:  %s\n", modbus_strerror(errno));
//                        res = 1;
//                        return "timeout device";
//                    }
//                    result_=result_+QString::number(tab_reg[0]);
//                    if(x< listAddr_.count()-1){result_+=",";};
//                }
                // baca modbus
                modbus_set_response_timeout(ctx, tv_sec,tv_usec);
                modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
               // int tempAlamat=QString(listAddr_.at(x)).toInt();
                  int max_=100;
                rc = modbus_read_registers(ctx,0, max_, tab_reg);

                if (rc == -1) {
                    printf("Read registers failed:  %s\n", modbus_strerror(errno));
                    res = 1;
                    modbus_close(ctx);
                    modbus_free(ctx);
                    return "timeout device";
                }

                for(int x=0;x<max_;x++)
                {
                    qDebug()<<QString::number(x)+" indeks , data="+QString::number(tab_reg[x]);
                    result_=result_+QString::number(tab_reg[x]);
                    if(x< max_-1){result_+=",";};
                }
                //return "ok";



                modbus_close(ctx);
                modbus_free(ctx);
                return result_;
            }
        }
        return "false";

    }

};

#endif // MW_H
