#ifndef MODBUSTHREAD_H
#define MODBUSTHREAD_H

#include <QObject>
#include <QMainWindow>
#include "3rdparty/libmodbus/src/modbus-rtu.h"
#include "3rdparty/libmodbus/src/modbus-tcp.h"
#include "3rdparty/libmodbus/src/modbus.h"

#include <errno.h>
#include <QDebug>
class modbusthread : public QObject
{
    Q_OBJECT
public:
    explicit modbusthread(QObject *parent = 0);

signals:
 void Signalmb_readIntHoldingResults(QString results,QStringList d);
 void Signalmb_readIntHoldingResultsWithDisplayOption(QString results,QStringList d);
public slots:
 void  Slotsmb_readIntHolding(int idSlave, QString listAddr , QStringList d);
 void  Slotsmb_readIntHoldingWithDisplayOption(int idSlave, QString listAddr , QString displayOption, QStringList d);

};

#endif // MODBUSTHREAD_H
