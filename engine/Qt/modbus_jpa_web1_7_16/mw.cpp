#include "mw.h"
#include "ui_mw.h"
#include <QSqlField>
#include <QThread>
long interval=10;
int addrFloat[5]={1,2,3,4,5};
#include "json.h"
QStringList mb_alamatRtu;
QStringList mb_alamatTcp;
QStringList mb_displayOption;
QThread *thModbus;
bool statusSimpanDetik;
bool db_olahSelectMysql(QString a)
{
//    QStringList listId=a.split("#");
//    qDebug() << listId.count()-1;
//    mb_alamatRtu.clear();
//    mb_displayOption.clear();
//    for(int x=0;x<listId.count()-1;x++)
//    {
//        QStringList listVal=listId.at(x).split(",");
//        // qDebug()<<listVal.count();
//        mb_alamatRtu.append(listVal.at(2)); //2 indeks  array database
//        mb_alamatTcp.append(listVal.at(3)); //3 indeks  array database
//        mb_displayOption.append(listId.at(x).at(4)); //4 indeks  array database
//         qDebug()<<listId.at(x).at(2) ;

//    }
//    for(int x=0;x<mb_displayOption.count();x++)
//    {


//    }

    QStringList listId=a.split("#");
   // qDebug() << listId.count()-1;
    mb_alamatRtu.clear();
    mb_displayOption.clear();
    for(int x=0;x<listId.count()-1;x++)
    {
        QStringList listVal=listId.at(x).split(",");
        // qDebug()<<listVal.count();
        mb_alamatRtu.append(listVal.at(2)); //2 indeks  array database
        mb_alamatTcp.append(listVal.at(3)); //3 indeks  array database
        mb_displayOption.append(listVal.at(4)); //4 indeks  array database
        // qDebug()<<listVal.at(4) ;

    }
    //  QMessageBox::information(this,"",a);
    return true;
}
QString readCsvAddr(){
    QFile file("daftarAlamat.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return "false";
    }
    QTextStream in(&file);
    QString line = in.readAll();
    if(!line.isEmpty()){
        return line;}
    return "false";

}

MW::MW(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MW)
{
    ui->setupUi(this);
    modbusthread_ = new modbusthread();
    connect(this,SIGNAL(Signalsmb_readIntHoldingToModbusThread(int,QString,QStringList)),
            modbusthread_,SLOT(Slotsmb_readIntHolding(int,QString,QStringList)));
    connect(modbusthread_,SIGNAL(Signalmb_readIntHoldingResults(QString,QStringList)),
            this,SLOT(Signalmb_readIntHoldingResultsFromModbusThread(QString,QStringList)));

    connect(this,SIGNAL(Signalsmb_readIntHoldingToModbusThreadWithDisplayOption(int,QString,QString,QStringList)),
            modbusthread_,SLOT(Slotsmb_readIntHoldingWithDisplayOption(int,QString,QString,QStringList)));
    connect(modbusthread_,SIGNAL(Signalmb_readIntHoldingResultsWithDisplayOption(QString,QStringList)),
            this,SLOT(Signalmb_readIntHoldingResultsFromModbusThreadWithDisplayOption(QString,QStringList)));
    thModbus=new QThread();
    modbusthread_->moveToThread(thModbus);
    thModbus->start();
    initMysql();
    db_selectAddrModbus();
    QTimer::singleShot(interval,this,SLOT(timUpdateModbus()));
    QTimer::singleShot(1000,this,SLOT(UpdateJam()));

    qDebug()<<"start";
    bool ok;
    // json is a QString containing the JSON data

}
bool updateMenitDb;
void MW::UpdateJam()
{
    QDateTime d;
    QString w = d.currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    ui->lblJam->setText(w);
    w=d.currentDateTime().toString("ss");
    if(w=="59"){
        // updateMenitDb=true;
        statusSimpanDetik=true;
    }
    QTimer::singleShot(1000,this,SLOT(UpdateJam()));
}

MW::~MW()
{  m_querythread->quit();
    m_querythread->wait();
    thModbus->quit();
    thModbus->wait();
    delete thModbus;
    delete m_querythread;
    delete m_model;
    delete ui;
}
/*!modbus thread*/
void MW::Signalmb_readIntHoldingResultsFromModbusThread(QString a,QStringList d){
    if(a.contains("OK")){
        a.replace("OK","");
        QStringList b= a.split(",");
        if(b.count()>2){
            if(statusSimpanDetik==true){
                statusSimpanDetik=false;
                db_simpan_valuePerMenit(a);
            }else{
                db_simpan_valueByid(a);
            }
        }
        QString c="";
        qDebug()<<"hasil= "+a;
        for(int x=0;x<d.count();x++){
            c=c+"400"+d.at(x)+":"+b.at(x)+"<br>";
        }
        ui->txtResponse->setText("DATAMODBUS="+c);
    }else{
        ui->txtResponse->setText("failed");
    }
    QTimer::singleShot(interval,this,SLOT(timUpdateModbus()));
}
void MW::Signalmb_readIntHoldingResultsFromModbusThreadWithDisplayOption(QString a,QStringList d){
    if(a.contains("OK")){
        a.replace("OK","");
        QStringList b= a.split(",");
        if(b.count()>2){
            if(statusSimpanDetik==true){
                statusSimpanDetik=false;
                db_simpan_valuePerMenit(a);
            }else{
                db_simpan_valueByid(a);
            }
        }
        QString c="";
        qDebug()<<"hasil= "+a;
        for(int x=0;x<d.count();x++){
            c=c+"400"+d.at(x)+":"+b.at(x)+"<br>";
        }
        ui->txtResponse->setText("DATAMODBUS="+c);
    }else{
        ui->txtResponse->setText("failed");
    }
    QTimer::singleShot(interval,this,SLOT(timUpdateModbus()));
}

/*end modbus thread*/
void MW::timUpdateModbus()
{
    db_selectAddrModbus();
}
void MW::on_pushButton_clicked()
{
    //read03(1,0,5);
    //    QString lis="5,2,7,1,4,6,8";
    //    mb_readIntHolding(1,lis);
    db_selectAddrModbus();

}

//!response database
void MW::slotGo()
{
    qsrand(QDateTime::currentMSecsSinceEpoch());
    ui->textBrowser->append("Running queries, please wait...");
    dispatch("device_registers", "select * from device_registers;");
    ui->textBrowser->append("Dispatched all queries.\n");
}

void MW::dispatch(const QString &queryId, const QString &query)
{
    ui->textBrowser->append(queryId +  ":: Executing:" + query);
    m_querythread->execute(queryId, query);
}
void MW::slotResults(const QString &queryId, const QList<QSqlRecord> &records, const QString &resultId)
{
    ui->textBrowser->append( QString("RESULTS: queryId: %1, resultId: %2, count: %3").arg(queryId).arg(resultId).arg(records.size()) );

    if (records.size() == 1){
        //   qDebug() << records.at(0);;
    }else if(records.size()>0){
        //   qDebug()<< QString("RESULTS: queryId: %1, resultId: %2, count: %3").arg(queryId).arg(resultId).arg(records.size()) ;

    }

    QString datak;
    if(queryId=="device_registers"){

        for(int x=0;x<records.size();x++){
            QSqlRecord rec = records.at(x);

            for(int i=0;i<rec.count();i++){
                QSqlField field=rec.field(i);
             //  qDebug()<<"fieldname"+ field.name()<<field.value().toString();
                datak=datak+field.value().toString();
                if(i<rec.count()-1){
                    datak+=",";
                }
            }
            datak+="#";
        }
        db_olahSelectMysql(datak);
    }else if(queryId=="ambilrtu"){

        for(int x=0;x<records.size();x++){
            QSqlRecord rec = records.at(x);

            for(int i=0;i<rec.count();i++){
                QSqlField field=rec.field(i);
             //   qDebug()<<field.name()<<field.value().toString();
                datak=datak+field.value().toString();
                if(i<rec.count()-1){
                    datak+=",";
                }
            }
            datak+="#";
        }
        db_olahSelectMysql(datak);
        // qDebug()<<"update modbus "+queryId;
        //  qDebug()<< "mb_alamatRtu"+QString::number(mb_alamatRtu.count());
        if(mb_alamatRtu.count()>1 ){
            QString lis;
            for(int x=0;x<mb_alamatRtu.count();x++)
            {

                if(x<mb_alamatRtu.count()-1){lis=lis+mb_alamatRtu.at(x)+",";}
                else{ lis+=mb_alamatRtu.at(x);}
                // qDebug()<<"ambil"+lis;
            }
            QStringList d=lis.split(",");
            emit this->Signalsmb_readIntHoldingToModbusThread(1,lis,d);

            //  qDebug()<<a;

        }else{
            QMessageBox::information(this,"","LIST modbus kosong");
        }


    }else if(queryId=="ambilrtuwithdisplayoption"){

        for(int x=0;x<records.size();x++){
            QSqlRecord rec = records.at(x);

            for(int i=0;i<rec.count();i++){
                QSqlField field=rec.field(i);
               //   qDebug()<<field.name()<<field.value().toString();
                datak=datak+field.value().toString();
                if(i<rec.count()-1){
                    datak+=",";
                }
            }
            datak+="#";
        }
        db_olahSelectMysql(datak);
       // qDebug()<<"update modbus "+queryId;
      //  qDebug()<< "mb_alamatRtu"+QString::number(mb_alamatRtu.count());
        if(mb_alamatRtu.count()>1 ){
            QString lis;
            QString displayOption;

            for(int x=0;x<mb_alamatRtu.count();x++)
            {

                if(x<mb_alamatRtu.count()-1){lis=lis+mb_alamatRtu.at(x)+",";}
                else{ lis+=mb_alamatRtu.at(x);}
                // qDebug()<<"ambil"+lis;
            }
            QStringList d=lis.split(",");
            for(int x=0;x<mb_displayOption.count();x++)
            {

                if(x<mb_displayOption.count()-1){displayOption=displayOption+mb_displayOption.at(x)+",";}
                else{ displayOption+=mb_displayOption.at(x);}
                // qDebug()<<"display option="+displayOption;
            }

            emit this->Signalsmb_readIntHoldingToModbusThreadWithDisplayOption(1,lis,displayOption,d);

            //  qDebug()<<a;

        }else{
            QMessageBox::information(this,"","LIST modbus kosong");
        }


    }


    m_model->setRecordList(records);

}
void MW::slotResultsErrorString(QString a, QString b, QString c)
{
    QMessageBox::information(this,"",b);
}
//!database
void MW::db_selectAddrModbus(){
    QString q="select * from device_registers";
    //dispatch("ambilrtu", q);
    dispatch("ambilrtuwithdisplayoption", q);

    ui->textBrowser->append("Dispatched all queries.\n");
}
void MW::db_selectAddrModbus2(QString j){
    QString q="select * from device_registers";
    dispatch(j, q);
    ui->textBrowser->append("Dispatched all queries.\n");
}
void MW::initMysql(){
    m_querythread = new QueryThread();

    // Buttons
    connect( ui->goButton, SIGNAL( clicked() ),
             this, SLOT( slotGo() ) );
    connect( ui->closeButton, SIGNAL( clicked() ),
             this, SLOT( close() ) );

    // Worker thread
    connect( m_querythread, SIGNAL(progress(QString)),
             ui->textBrowser, SLOT(append(QString)) );
    connect( m_querythread, SIGNAL( ready(bool) ),
             ui->goButton, SLOT( setEnabled(bool) ) );
    connect( m_querythread, SIGNAL(results(QString,QList<QSqlRecord>,QString)),
             this, SLOT(slotResults(QString,QList<QSqlRecord>,QString)) );
    connect(m_querythread,SIGNAL(executeFailedString(QString,QString,QString)),
            this,SLOT(slotResultsErrorString(QString,QString,QString)));

    // Launch worker thread
    m_querythread->start();

    m_model = new SqlRecModel(this);
    ui->tableView->setModel(m_model);
}
void MW::db_simpan_valueByid(QString val)
{
    QDateTime d;
    QString waktu = d.currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    QStringList b= val.split(",");
    for(int x=0;x<b.count();x++){
        //QString q = "UPDATE device_registers SET register_value_='"+QString(b.at(x))+"' WHERE id = :value";
        QString q = "UPDATE device_registers SET updated_at='"+ waktu +"',register_value_='"+QString(b.at(x))+"' WHERE id ="+QString::number(x+1)+";";
        dispatch(QString::number(x+1), q);
        //m_querythread->prepare(QString::number(x), q);
        //m_querythread->bindValue(QString::number(x), ":value", x);
        //m_querythread->executePrepared(QString::number(x));
    }
    ui->textBrowser->append("Running queries, please wait...");

    //          dispatch("model", QString("select id, name from item limit 100 offset %1").arg(rand) );
    //      textBrowser->append("Dispatched all queries.\n");


}
void MW::db_simpan_valuePerMenit(QString val)
{
    QDateTime d;
    QString waktu = d.currentDateTime().toString("yyyy-MM-dd hh:mm");
    waktu=waktu+":59";
    QStringList b= val.split(",");
    QString val_="";
    for(int x=0;x<b.count()-1;x++){
        val_=val_+"'"+b.at(x)+"',";
    }
    QString idWaktu="'"+d.currentDateTime().toString("yyyyMMddhhmm")+"59',";
    QString q="INSERT INTO device_value_minutes (id, Phase_1_Voltage_LN, Phase_2_Voltage_LN, Phase_3_Voltage_LN, "
              "Average_Voltage_LN, Phase_1_Voltage_LL, Phase_2_Voltage_LL, Phase_3_Voltage_LL, Average_Voltage_LL,"
              " Phase_1_Current, Phase_2_Current, Phase_3_Current, Total_Current, Phase_1_cos_pi, Phase_2_cos_pi, "
              "Phase_3_cos_pi, Phase_1_Power_Factor, Phase_2_Power_Factor, Phase_3_Power_Factor, System_Power_Factor,"
              " Phase_1_Active_Power, Phase_2_Active_Power, Phase_3_Active_Power, Total_Active_Power, Phase_1_Reactive_Power,"
              " Phase_2_Reactive_Power, Phase_3_Reactive_Power, Total_Reactive_Power, Phase_1_Apparent_Power, Phase_2_Apparent_Power,"
              " Phase_3_Apparent_Power, Total_Apparent_Power, Phase_1_THDV, Phase_2_THDV, Phase_3_THDV, Phase_1_THDI, Phase_2_THDI,"
              " Phase_3_THDI, System_Frequency, Neutral_Current, Export_Active_Energy_T1, Export_Reactive_Energy_T1, waktu) "
              "VALUES ( "+idWaktu+val_+"'"+ waktu+"')";
    dispatch("update menit", q);

    // QMessageBox::information(this,"",idWaktu);

    ui->textBrowser->append("Running queries, please wait...");

    //          dispatch("model", QString("select id, name from item limit 100 offset %1").arg(rand) );
    //      textBrowser->append("Dispatched all queries.\n");



}

void MW::on_pbTestDatabase_clicked()
{
    statusSimpanDetik=true;
    db_selectAddrModbus();
}
