#include "modbusthread.h"
#include <algorithm>
#include <stdio.h>      /* printf */
#include <stdlib.h>     /* qsort */
int compare (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}
modbusthread::modbusthread(QObject *parent) : QObject(parent)
{

}
void modbusthread::Slotsmb_readIntHolding(int idSlave, QString listAddr , QStringList d)
{
    modbus_t *ctx;
    uint16_t tab_reg[65535] ;
    float avgVLL = -1;;
    int res = 0;
    int rc;
    int i;
    struct timeval response_timeout;
    uint32_t tv_sec = 0;
    uint32_t tv_usec = 0;
    //response_timeout.tv_sec = 10;// timeoutnya
    //response_timeout.tv_usec = 0;
    setbuf(stdout, NULL);
    qDebug()<<"connect prepare";
#ifdef WIN32
    // Windows code here
    ctx = modbus_new_rtu("COM8", 38400, 'N', 8, 1);
#else
    // UNIX code here
    ctx = modbus_new_rtu("/dev/ttyAMA0", 9600, 'N', 8, 1);
#endif

    if (NULL == ctx)
    {
        qDebug()<<("Unable to create libmodbus context\n");
        res = 1;
        emit Signalmb_readIntHoldingResults( "false",d);
    }
    else
    {
        printf("created libmodbus context\n");
        modbus_set_debug(ctx, FALSE);
        //modbus_set_error_recovery(ctx, MODBUS_ERROR_RECOVERY_LINK |MODBUS_ERROR_RECOVERY_PROTOCOL);
        rc = modbus_set_slave(ctx, idSlave);
        printf("modbus_set_slave return: %d\n",rc);
        if (rc != 0)
        {
            printf("modbus_set_slave: %s \n",modbus_strerror(errno));
            modbus_close(ctx);
            modbus_free(ctx);
            return;
        }


        /* This code is for version 3.1.2*/
        modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
        printf("Default response timeout:%d sec %d usec \n",tv_sec,tv_usec );

        tv_sec = 10;// timeoutnya
        tv_usec = 0;

        modbus_set_response_timeout(ctx, tv_sec,tv_usec);
        modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
        printf("Set response timeout:%d sec %d usec \n",tv_sec,tv_usec );


        rc = modbus_connect(ctx);
        printf("modbus_connect: %d \n",rc);

        if (rc == -1) {
            printf("Connection failed: %s\n", modbus_strerror(errno));
            res = 1;
            modbus_close(ctx);
            modbus_free(ctx);
            emit Signalmb_readIntHoldingResults( "port salah",d);
            return;
        }else{

            QStringList listAddr_=listAddr.split(",");

            // AMBIL LIST ALAMAT dan data satu2
            QString result_="";
            modbus_set_response_timeout(ctx, tv_sec,tv_usec);
            modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
            // int tempAlamat=QString(listAddr_.at(x)).toInt();
            int max_=100;
            rc = modbus_read_registers(ctx,0, max_, tab_reg);

            if (rc == -1) {
                printf("Read registers failed:  %s\n", modbus_strerror(errno));
                res = 1;
                modbus_close(ctx);
                modbus_free(ctx);
                emit Signalmb_readIntHoldingResults( "timeout device",d);
                return;
            }
            for(int x=0;x<listAddr_.count();x++)
            {
                int addr=QString(listAddr_.at(x)).toInt();
                result_=result_+QString::number(tab_reg[addr]);
                if(x< max_-1){result_+=",";};

            }
            modbus_close(ctx);
            modbus_free(ctx);
            emit Signalmb_readIntHoldingResults("OK"+result_,d);
            return;
        }
    }

}
//
void modbusthread::Slotsmb_readIntHoldingWithDisplayOption(int idSlave, QString listAddr ,QString displayOption , QStringList d)
{
    modbus_t *ctx;
    uint16_t tab_reg[65535] ;
    float avgVLL = -1;;
    int res = 0;
    int rc;
    int i;
    struct timeval response_timeout;
    uint32_t tv_sec = 0;
    uint32_t tv_usec = 0;
    //response_timeout.tv_sec = 10;// timeoutnya
    //response_timeout.tv_usec = 0;
    setbuf(stdout, NULL);
    qDebug()<<"connect prepare";
#ifdef WIN32
    // Windows code here
    ctx = modbus_new_rtu("COM8", 38400, 'N', 8, 1);
#else
    // UNIX code here
    ctx = modbus_new_rtu("/dev/ttyAMA0", 9600, 'N', 8, 1);
#endif

    if (NULL == ctx)
    {
        qDebug()<<("Unable to create libmodbus context\n");
        res = 1;
        emit Signalmb_readIntHoldingResults( "false",d);
    }
    else
    {
        printf("created libmodbus context\n");
        modbus_set_debug(ctx, FALSE);
        //modbus_set_error_recovery(ctx, MODBUS_ERROR_RECOVERY_LINK |MODBUS_ERROR_RECOVERY_PROTOCOL);
        rc = modbus_set_slave(ctx, idSlave);
        printf("modbus_set_slave return: %d\n",rc);
        if (rc != 0)
        {
            printf("modbus_set_slave: %s \n",modbus_strerror(errno));
            modbus_close(ctx);
            modbus_free(ctx);
            return;
        }


        /* This code is for version 3.1.2*/
        modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
        printf("Default response timeout:%d sec %d usec \n",tv_sec,tv_usec );

        tv_sec = 10;// timeoutnya
        tv_usec = 0;

        modbus_set_response_timeout(ctx, tv_sec,tv_usec);
        modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
        printf("Set response timeout:%d sec %d usec \n",tv_sec,tv_usec );


        rc = modbus_connect(ctx);
        printf("modbus_connect: %d \n",rc);

        if (rc == -1) {
            printf("Connection failed: %s\n", modbus_strerror(errno));
            res = 1;
            modbus_close(ctx);
            modbus_free(ctx);
            emit Signalmb_readIntHoldingResults( "port salah",d);
            return;
        }else{

            QStringList listAddr_=listAddr.split(",");

            // AMBIL LIST ALAMAT dan data satu2
            QString result_="";
            modbus_set_response_timeout(ctx, tv_sec,tv_usec);
            modbus_get_response_timeout(ctx, &tv_sec, &tv_usec);
            // int tempAlamat=QString(listAddr_.at(x)).toInt();
            int max_=120;
            rc = modbus_read_registers(ctx,0, max_, tab_reg);

            if (rc == -1) {
                printf("Read registers failed:  %s\n", modbus_strerror(errno));
                res = 1;
                modbus_close(ctx);
                modbus_free(ctx);
                emit Signalmb_readIntHoldingResults( "timeout device",d);
                return;
            }
            QStringList listOption_=displayOption.split(",");
            for(int x=0;x<listAddr_.count();x++)
            {
                int addr=QString(listAddr_.at(x)).toInt();
                QString listOption=listOption_.at(x);
                qDebug()<<listOption;
                if(listOption=="int16"){
                    result_=result_+QString::number(tab_reg[addr]);
                }else if(listOption=="float32"){
                    uint16_t tab_regFloat32[3]={tab_reg[addr],tab_reg[addr+1]} ;
                    float tempFloat32=modbus_get_float_dcba(tab_regFloat32);
                    result_=result_+QString::number(tempFloat32,'f',3);
                }
                if(x< max_-1){result_+=",";};

            }
            qDebug()<<"sundul";
            modbus_close(ctx);
            modbus_free(ctx);
            emit Signalmb_readIntHoldingResults("OK"+result_,d);
            return;
        }
    }

}
