#ifndef DB_H
#define DB_H

// Configure runtime parameters here 
#define DATABASE_NAME "jpa"
#define DATABASE_HOST "localhost"
#define DATABASE_DRIVER "QMYSQL"
#define SAMPLE_RECORDS 1000000

#endif
