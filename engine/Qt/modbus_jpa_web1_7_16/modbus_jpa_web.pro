#-------------------------------------------------
#
# Project created by QtCreator 2016-06-24T18:20:48
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = modbus_jpa_web
TEMPLATE = app
CONFIG += extserialport
CONFIG += qt thread
INCLUDEPATH +=3rdparty/libmodbus/src
   # include(3rdparty/qextserialport/src/qextserialport.pri)
include(json/qt-json.pri)
unix {
    SOURCES += 3rdparty/qextserialport/posix_qextserialport.cpp	\
           3rdparty/qextserialport/qextserialenumerator_unix.cpp
    DEFINES += _TTY_POSIX_
}

win32 {
#    SOURCES += 3rdparty/qextserialport/win_qextserialport.cpp \
#           3rdparty/qextserialport/qextserialenumerator_win.cpp
    DEFINES += _TTY_WIN_  WINVER=0x0501
    LIBS += -lsetupapi -lws2_32
}
LIBS +=  –libs –cflags
SOURCES += main.cpp\
        mw.cpp \
    3rdparty/libmodbus/src/modbus.c \
    3rdparty/libmodbus/src/modbus-data.c \
    3rdparty/libmodbus/src/modbus-rtu.c \
    3rdparty/libmodbus/src/modbus-tcp.c \
    sql/querythread.cpp \
    sql/sqlrecmodel.cpp \
    modbusthread.cpp

HEADERS  += mw.h \
    3rdparty/libmodbus/src/config.h \
    3rdparty/libmodbus/src/modbus.h \
    3rdparty/libmodbus/src/modbus-private.h \
    3rdparty/libmodbus/src/modbus-rtu.h \
    3rdparty/libmodbus/src/modbus-rtu-private.h \
    3rdparty/libmodbus/src/modbus-tcp.h \
    3rdparty/libmodbus/src/modbus-tcp-private.h \
    3rdparty/libmodbus/src/modbus-version.h \
    sql/db.h \
    sql/querythread.h \
    sql/sqlrecmodel.h \
    modbusthread.h

FORMS    += mw.ui
