/****************************************************************************
** Meta object code from reading C++ file 'querythread.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../sql/querythread.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'querythread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Worker[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,    8,    7,    7, 0x05,
      72,   51,    7,    7, 0x05,
     121,  113,    7,    7, 0x05,
     151,  139,    7,    7, 0x05,
     209,  184,    7,    7, 0x05,
     252,   51,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
     309,  297,    7,    7, 0x0a,
     338,    8,    7,    7, 0x0a,
     375,  113,    7,    7, 0x2a,
     404,  297,    7,    7, 0x0a,
     457,  433,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Worker[] = {
    "Worker\0\0queryId,resultId\0"
    "executed(QString,QString)\0"
    "queryId,err,resultId\0"
    "executeFailed(QString,QSqlError,QString)\0"
    "queryId\0prepared(QString)\0queryId,err\0"
    "prepareFailed(QString,QSqlError)\0"
    "queryId,records,resultId\0"
    "results(QString,QList<QSqlRecord>,QString)\0"
    "executeFailedString(QString,QString,QString)\0"
    "queryId,sql\0slotExecute(QString,QString)\0"
    "slotExecutePrepared(QString,QString)\0"
    "slotExecutePrepared(QString)\0"
    "slotPrepare(QString,QString)\0"
    "queryId,placeholder,val\0"
    "slotBindValue(QString,QString,QVariant)\0"
};

void Worker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Worker *_t = static_cast<Worker *>(_o);
        switch (_id) {
        case 0: _t->executed((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 1: _t->executeFailed((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QSqlError(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 2: _t->prepared((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->prepareFailed((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QSqlError(*)>(_a[2]))); break;
        case 4: _t->results((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QList<QSqlRecord>(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 5: _t->executeFailedString((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 6: _t->slotExecute((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 7: _t->slotExecutePrepared((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 8: _t->slotExecutePrepared((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->slotPrepare((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 10: _t->slotBindValue((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QVariant(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Worker::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Worker::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Worker,
      qt_meta_data_Worker, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Worker::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Worker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Worker::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Worker))
        return static_cast<void*>(const_cast< Worker*>(this));
    return QObject::qt_metacast(_clname);
}

int Worker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void Worker::executed(const QString & _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Worker::executeFailed(const QString & _t1, const QSqlError & _t2, const QString & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Worker::prepared(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Worker::prepareFailed(const QString & _t1, const QSqlError & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Worker::results(const QString & _t1, const QList<QSqlRecord> & _t2, const QString & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Worker::executeFailedString(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
static const uint qt_meta_data_QueryThread[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   13,   12,   12, 0x05,
      35,   12,   12,   12, 0x05,
      64,   47,   12,   12, 0x05,
     111,   90,   12,   12, 0x05,
     152,   90,   12,   12, 0x05,
     205,  197,   12,   12, 0x05,
     235,  223,   12,   12, 0x05,
     293,  268,   12,   12, 0x05,
     348,  336,   12,   12, 0x05,
     376,   47,   12,   12, 0x05,
     412,  197,   12,   12, 0x25,
     440,  336,   12,   12, 0x05,
     492,  468,   12,   12, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QueryThread[] = {
    "QueryThread\0\0msg\0progress(QString)\0"
    "ready(bool)\0queryId,resultId\0"
    "executed(QString,QString)\0"
    "queryId,err,resultId\0"
    "executeFailed(QString,QSqlError,QString)\0"
    "executeFailedString(QString,QString,QString)\0"
    "queryId\0prepared(QString)\0queryId,err\0"
    "prepareFailed(QString,QSqlError)\0"
    "queryId,records,resultId\0"
    "results(QString,QList<QSqlRecord>,QString)\0"
    "queryId,sql\0fwdExecute(QString,QString)\0"
    "fwdExecutePrepared(QString,QString)\0"
    "fwdExecutePrepared(QString)\0"
    "fwdPrepare(QString,QString)\0"
    "queryId,placeholder,val\0"
    "fwdBindValue(QString,QString,QVariant)\0"
};

void QueryThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QueryThread *_t = static_cast<QueryThread *>(_o);
        switch (_id) {
        case 0: _t->progress((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->ready((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->executed((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 3: _t->executeFailed((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QSqlError(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 4: _t->executeFailedString((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 5: _t->prepared((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->prepareFailed((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QSqlError(*)>(_a[2]))); break;
        case 7: _t->results((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QList<QSqlRecord>(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 8: _t->fwdExecute((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 9: _t->fwdExecutePrepared((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 10: _t->fwdExecutePrepared((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->fwdPrepare((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 12: _t->fwdBindValue((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QVariant(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QueryThread::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QueryThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_QueryThread,
      qt_meta_data_QueryThread, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QueryThread::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QueryThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QueryThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QueryThread))
        return static_cast<void*>(const_cast< QueryThread*>(this));
    return QThread::qt_metacast(_clname);
}

int QueryThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void QueryThread::progress(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QueryThread::ready(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QueryThread::executed(const QString & _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QueryThread::executeFailed(const QString & _t1, const QSqlError & _t2, const QString & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QueryThread::executeFailedString(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QueryThread::prepared(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QueryThread::prepareFailed(const QString & _t1, const QSqlError & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QueryThread::results(const QString & _t1, const QList<QSqlRecord> & _t2, const QString & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QueryThread::fwdExecute(const QString & _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QueryThread::fwdExecutePrepared(const QString & _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 11
void QueryThread::fwdPrepare(const QString & _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void QueryThread::fwdBindValue(const QString & _t1, const QString & _t2, const QVariant & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}
QT_END_MOC_NAMESPACE
