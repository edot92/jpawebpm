/****************************************************************************
** Meta object code from reading C++ file 'mw.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mw.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mw.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MW[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
       4,    3,    3,    3, 0x05,
      37,   18,    3,    3, 0x05,
     134,  101,    3,    3, 0x05,

 // slots: signature, parameters, type, tag, flags
     223,    3,    3,    3, 0x08,
     247,    3,    3,    3, 0x08,
     265,    3,    3,    3, 0x08,
     299,  274,    3,    3, 0x08,
     349,  346,    3,    3, 0x08,
     400,  397,    3,    3, 0x08,
     468,  397,    3,    3, 0x08,
     553,    3,    3,    3, 0x08,
     565,    3,    3,    3, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MW[] = {
    "MW\0\0exec(QString)\0idSlave,listAddr,d\0"
    "Signalsmb_readIntHoldingToModbusThread(int,QString,QStringList)\0"
    "idSlave,listAddr,displayOption,d\0"
    "Signalsmb_readIntHoldingToModbusThreadWithDisplayOption(int,QString,QS"
    "tring,QStringList)\0"
    "on_pushButton_clicked()\0timUpdateModbus()\0"
    "slotGo()\0queryId,records,resultId\0"
    "slotResults(QString,QList<QSqlRecord>,QString)\0"
    ",,\0slotResultsErrorString(QString,QString,QString)\0"
    ",d\0"
    "Signalmb_readIntHoldingResultsFromModbusThread(QString,QStringList)\0"
    "Signalmb_readIntHoldingResultsFromModbusThreadWithDisplayOption(QStrin"
    "g,QStringList)\0"
    "UpdateJam()\0on_pbTestDatabase_clicked()\0"
};

void MW::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MW *_t = static_cast<MW *>(_o);
        switch (_id) {
        case 0: _t->exec((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->Signalsmb_readIntHoldingToModbusThread((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QStringList(*)>(_a[3]))); break;
        case 2: _t->Signalsmb_readIntHoldingToModbusThreadWithDisplayOption((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QStringList(*)>(_a[4]))); break;
        case 3: _t->on_pushButton_clicked(); break;
        case 4: _t->timUpdateModbus(); break;
        case 5: _t->slotGo(); break;
        case 6: _t->slotResults((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QList<QSqlRecord>(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 7: _t->slotResultsErrorString((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 8: _t->Signalmb_readIntHoldingResultsFromModbusThread((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QStringList(*)>(_a[2]))); break;
        case 9: _t->Signalmb_readIntHoldingResultsFromModbusThreadWithDisplayOption((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QStringList(*)>(_a[2]))); break;
        case 10: _t->UpdateJam(); break;
        case 11: _t->on_pbTestDatabase_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MW::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MW::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MW,
      qt_meta_data_MW, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MW::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MW::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MW::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MW))
        return static_cast<void*>(const_cast< MW*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MW::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void MW::exec(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MW::Signalsmb_readIntHoldingToModbusThread(int _t1, QString _t2, QStringList _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MW::Signalsmb_readIntHoldingToModbusThreadWithDisplayOption(int _t1, QString _t2, QString _t3, QStringList _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
