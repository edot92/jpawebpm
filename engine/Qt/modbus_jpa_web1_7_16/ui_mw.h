/********************************************************************************
** Form generated from reading UI file 'mw.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MW_H
#define UI_MW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QTableView>
#include <QtGui/QTextBrowser>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MW
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QTextEdit *txtResponse;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *_2;
    QSpacerItem *spacerItem;
    QPushButton *closeButton;
    QGridLayout *gridLayout;
    QTextBrowser *textBrowser;
    QTableView *tableView;
    QLabel *label;
    QLabel *label_2;
    QHBoxLayout *_3;
    QPushButton *goButton;
    QSpacerItem *spacerItem1;
    QLabel *lblJam;
    QPushButton *pbTestDatabase;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MW)
    {
        if (MW->objectName().isEmpty())
            MW->setObjectName(QString::fromUtf8("MW"));
        MW->resize(1129, 545);
        centralWidget = new QWidget(MW);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(20, 0, 75, 23));
        txtResponse = new QTextEdit(centralWidget);
        txtResponse->setObjectName(QString::fromUtf8("txtResponse"));
        txtResponse->setGeometry(QRect(0, 30, 281, 401));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(300, 20, 522, 277));
        verticalLayout_4 = new QVBoxLayout(layoutWidget);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        _2 = new QHBoxLayout();
        _2->setSpacing(6);
        _2->setObjectName(QString::fromUtf8("_2"));
        _2->setContentsMargins(0, 0, 0, 0);
        spacerItem = new QSpacerItem(261, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        _2->addItem(spacerItem);

        closeButton = new QPushButton(layoutWidget);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        closeButton->setDefault(true);

        _2->addWidget(closeButton);


        verticalLayout_4->addLayout(_2);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        textBrowser = new QTextBrowser(layoutWidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setAcceptRichText(false);
        textBrowser->setTextInteractionFlags(Qt::NoTextInteraction);

        gridLayout->addWidget(textBrowser, 1, 0, 1, 1);

        tableView = new QTableView(layoutWidget);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        gridLayout->addWidget(tableView, 1, 1, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 1, 1, 1);


        verticalLayout_4->addLayout(gridLayout);

        _3 = new QHBoxLayout();
        _3->setSpacing(6);
        _3->setObjectName(QString::fromUtf8("_3"));
        _3->setContentsMargins(0, 0, 0, 0);
        goButton = new QPushButton(layoutWidget);
        goButton->setObjectName(QString::fromUtf8("goButton"));

        _3->addWidget(goButton);

        spacerItem1 = new QSpacerItem(271, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        _3->addItem(spacerItem1);


        verticalLayout_4->addLayout(_3);

        lblJam = new QLabel(centralWidget);
        lblJam->setObjectName(QString::fromUtf8("lblJam"));
        lblJam->setGeometry(QRect(110, 0, 231, 16));
        pbTestDatabase = new QPushButton(centralWidget);
        pbTestDatabase->setObjectName(QString::fromUtf8("pbTestDatabase"));
        pbTestDatabase->setGeometry(QRect(310, 330, 75, 23));
        MW->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MW);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1129, 21));
        MW->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MW);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MW->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MW);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MW->setStatusBar(statusBar);

        retranslateUi(MW);

        QMetaObject::connectSlotsByName(MW);
    } // setupUi

    void retranslateUi(QMainWindow *MW)
    {
        MW->setWindowTitle(QApplication::translate("MW", "MW", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("MW", "PushButton", 0, QApplication::UnicodeUTF8));
        txtResponse->setHtml(QApplication::translate("MW", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">tes</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", 0, QApplication::UnicodeUTF8));
        closeButton->setText(QApplication::translate("MW", "&Close", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MW", "Query Progress Log", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MW", "SqlRecModel", 0, QApplication::UnicodeUTF8));
        goButton->setText(QApplication::translate("MW", "&Go", 0, QApplication::UnicodeUTF8));
        lblJam->setText(QApplication::translate("MW", "TextLabel", 0, QApplication::UnicodeUTF8));
        pbTestDatabase->setText(QApplication::translate("MW", "test databse", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MW: public Ui_MW {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MW_H
