��    +      t  ;   �      �     �  	   �     �     �  ?   �     6     ;  t   U     �     �     �          3     L     ]  -   u     �     �  $   �  $   �       )     !   ?     a  &   x     �     �     �     �     �     �     �          *     C     c     |     �  
   �     �  
   �     �  �  �  %   �	  	   �	     �	     �	  G   �	     A
     I
  }   f
     �
     �
          4     L     e     |  #   �     �     �  &   �  &   �       B   1     t     �  0   �     �     �     �     
          5     Q  %   g     �  $   �     �  
   �     �          !     6     E        %               $   +       !                    #                                               )                               (   '   &          	            "                 *      
               32 Bit float or annotation About ... About FPconvert Convert Could not find/load the "FPconvert.mo" catalog for locale "%s". Exit Floating Point Conversion Floating Point Conversion
FPconvert Version 1.08
(c) Copyright 2004-2013 Ing.-Buro Dr. Friedrich Haase
www.61131.com HEX Bytes, 32 Bit float HEX Bytes, 64 Bit double HEX Double Words, 64 Bit double HEX Words, 32 Bit float HEX Words, 64 Bit double Intel Byte order Locale "%s" is unknown. This language is not supported by the system. Value about FPconvert convert the 32 Bit hexadecimal value convert the 64 Bit hexadecimal value convert the value could not set C locale for numeric values enter a floating-point value here exponent (hexadecimal) exponent base 2, decimal, without bias invalid value locale one of eight Bytes one of four Bytes one of four words one of two dwords one of two words select for Intel Byte order select to swap all Bytes select to swap all double words select to swap all words sign significand (hexadecimal) swap Bytes swap double words swap words terminate FPconvert Project-Id-Version: FPconvert 1.05
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-04 09:25+0100
PO-Revision-Date: 2013-07-04 09:25+0100
Last-Translator: Dr. Friedrich Haase <friedrich.haase@61131.com>
Language-Team: FHA <Friedrich@61131.com>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: \work\wx
X-Generator: Poedit 1.5.6
X-Poedit-SearchPath-0: FPconvert
 32 Bit Fließkommawert oder Anmerkung Über ... über FPconvert Konvertieren Konnte den Katalog "FPconvert.mo" für Sprache "%s" nicht finden/laden. Beenden Fließkommaformat-Umwandlung Fließkommaformat-Umwandlung
FPconvert Version 1.08 (de)
(c) Copyright 2004-2013 Ing.-Büro Dr. Friedrich Haase
www.61131.com HEX Bytes, 32 Bit float HEX Bytes, 64 Bit double HEX Doppelworte, 64 Bit double HEX Worte, 32 Bit float HEX Worte, 64 Bit double Intel Byte Reihenfolge Sprache "%s" ist unbekannt. Die Sprache ist nicht unterstützt. Wert über FPconvert 32 Bit hexadezimalen Wert konvertieren 64 Bit hexadezimalen Wert konvertieren Wert konvertieren Einstellung für numerische Werte (C locale) konnte nicht erfolgen Fließkommawert hier eingeben Exponent (hexadezimal) Exponent zur Basis 2, dezimal, ohne Verschiebung unzulässiger Wert Sprache eines von acht Bytes eines von vier Bytes eines von vier Worten eines von zwei Doppelworten eines von zwei Worten markieren für Intel Byte Reihenfolge markieren um Bytes zu tauschen markieren um Doppelworte zu tauschen markieren um Worte zu tauschen Vorzeichen Mantisse (hexadezimal) Bytes tauschen Doppelworte tauschen Worte tauschen FPconvert beenden 