��    +      t  ;   �      �     �  	   �     �     �  ?   �     6     ;  t   U     �     �     �          3     L     ]  -   u     �     �  $   �  $   �       )     !   ?     a  &   x     �     �     �     �     �     �     �          *     C     c     |     �  
   �     �  
   �     �  �  �     �	  	   �	     �	     �	  ?   �	     )
     .
  x   H
     �
     �
      �
          +     D     U  -   m     �     �  $   �  $   �     �  )     !   7     Y  &   p     �     �     �     �     �     �     �          "     ;     [     t     y  
   �     �  
   �     �        %               $   +       !                    #                                               )                               (   '   &          	            "                 *      
               32 Bit float or annotation About ... About FPconvert Convert Could not find/load the "FPconvert.mo" catalog for locale "%s". Exit Floating Point Conversion Floating Point Conversion
FPconvert Version 1.08
(c) Copyright 2004-2013 Ing.-Buro Dr. Friedrich Haase
www.61131.com HEX Bytes, 32 Bit float HEX Bytes, 64 Bit double HEX Double Words, 64 Bit double HEX Words, 32 Bit float HEX Words, 64 Bit double Intel Byte order Locale "%s" is unknown. This language is not supported by the system. Value about FPconvert convert the 32 Bit hexadecimal value convert the 64 Bit hexadecimal value convert the value could not set C locale for numeric values enter a floating-point value here exponent (hexadecimal) exponent base 2, decimal, without bias invalid value locale one of eight Bytes one of four Bytes one of four words one of two dwords one of two words select for Intel Byte order select to swap all Bytes select to swap all double words select to swap all words sign significand (hexadecimal) swap Bytes swap double words swap words terminate FPconvert Project-Id-Version: FPconvert 1.05
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-04 09:25+0100
PO-Revision-Date: 2013-07-04 09:26+0100
Last-Translator: Dr. Friedrich Haase <friedrich.haase@61131.com>
Language-Team: FHA <Friedrich@61131.com>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: \work\wx
X-Generator: Poedit 1.5.6
X-Poedit-SearchPath-0: FPconvert
 32 Bit float or annotation About ... About FPconvert Convert Could not find/load the "FPconvert.mo" catalog for locale "%s". Exit Floating Point Conversion Floating Point Conversion
FPconvert Version 1.08(en)
(c) Copyright 2004-2013 Ing.-Buro Dr. Friedrich Haase
www.61131.com HEX Bytes, 32 Bit float HEX Bytes, 64 Bit double HEX Double Wordss, 64 Bit double HEX Words, 32 Bit float HEX Words, 64 Bit double Intel Byte order Locale "%s" is unknown. This language is not supported by the system. Value about FPconvert convert the 32 Bit hexadecimal value convert the 64 Bit hexadecimal value convert the value could not set C locale for numeric values enter a floating-point value here exponent (hexadecimal) exponent base 2, decimal, without bias invalid value locale one of eight Bytes one of four Bytes one of four words one of two dwords one of two words select for Intel Byte order select to swap all Bytes select to swap all double words select to swap all words sign significand (hexadecimal) swap Bytes swap double words swap words terminate FPconvert 