@extends('adminlte::page')

@section('title', 'PROJECT JPA')

@section('content_header')
<h1>USER MANAJEMEN</h1>
@stop


@section('content')
<div class="container">

    <h1>user {{ $user->id }}
        <a href="{{ url('user/' . $user->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit user"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['user', $user->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete user',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $user->id }}</td>
                </tr>
                <tr><th> Name </th><td> {{ $user->name }} </td></tr><tr><th> Email  </th><td> {{ $user->email}} </td></tr><tr><th> Last Password </th><td> {{ $user->last_password }} </td></tr><tr><th> Newpassword </th><td> {{ $user->newpassword }} </td></tr><tr><th> Admin </th><td> {{ $user->admin }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
