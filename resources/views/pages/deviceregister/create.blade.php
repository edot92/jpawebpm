@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Create New deviceregister</h1>
    <hr/>

    {!! Form::open(['url' => '/deviceregister', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('register_label_') ? 'has-error' : ''}}">
                {!! Form::label('register_label_', 'Register Label ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('register_label_', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('register_label_', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('register_rtu_') ? 'has-error' : ''}}">
                {!! Form::label('register_rtu_', 'Register Rtu ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('register_rtu_', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('register_rtu_', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('register_tcp_') ? 'has-error' : ''}}">
                {!! Form::label('register_tcp_', 'Register Tcp ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('register_tcp_', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('register_tcp_', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('display_option_') ? 'has-error' : ''}}">
                {!! Form::label('display_option_', 'Display Option ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('display_option_', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('display_option_', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('register_multiply_') ? 'has-error' : ''}}">
                {!! Form::label('register_multiply_', 'Register Multiply ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('register_multiply_', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('register_multiply_', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

</div>
@endsection