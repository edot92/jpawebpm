@extends('adminlte::page')

@section('title', 'PROJECT JPA')

@section('content_header')
<h1>REGISTER DEVICES</h1>
@stop

@section('content')
<div class="container">
<!-- 
    <h1>Deviceregister <a href="{{ url('/deviceregister/create') }}" class="btn btn-primary btn-xs" title="Add New deviceregister"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1> -->
   <div class="table-responsive">
        <div class="table">
            <table class="table table-bordered table-striped table-hover" style="width: 89%;"">
                <thead>
                    <tr>
                        <th>No</th><th> Register Label  </th><th> Register Rtu  </th><th> Register Tcp  </th><th> Display Option  </th><th> Register Multiply </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($deviceregister as $item)
                    {{-- */$x++;/* --}}
                    <tr>
                     <!--    <td>{{ $x }}</td> -->
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->register_label_ }}</td><td>{{ $item->register_rtu_ }}</td><td>{{ $item->register_tcp_ }}</td><td>{{ $item->display_option_ }}</td><td>{{ $item->register_multiply_ }}</td>
                        <td>
                           <!--  <a href="{{ url('/deviceregister/' . $item->id) }}" class="btn btn-success btn-xs" title="View deviceregister"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a> -->
                            <a href="{{ url('/deviceregister/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit deviceregister"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                         <!--    {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/deviceregister', $item->id],
                                'style' => 'display:inline'
                                ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete deviceregister" />', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete deviceregister',
                                'onclick'=>'return confirm("Confirm delete?")'
                                ));!!}
                                {!! Form::close() !!} -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $deviceregister->render() !!} </div>
            </div>
        </div>
    </div>
    @endsection
