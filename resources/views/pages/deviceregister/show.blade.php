@extends('layouts.app')

@section('content')
<div class="container">

    <h1>deviceregister {{ $deviceregister->id }}
        <a href="{{ url('deviceregister/' . $deviceregister->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit deviceregister"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['deviceregister', $deviceregister->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete deviceregister',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $deviceregister->id }}</td>
                </tr>
                <tr><th> Register Label  </th><td> {{ $deviceregister->register_label_ }} </td></tr><tr><th> Register Rtu  </th><td> {{ $deviceregister->register_rtu_ }} </td></tr><tr><th> Register Tcp  </th><td> {{ $deviceregister->register_tcp_ }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
