@extends('adminlte::page')

@section('title', 'PROJECT AKMI SIMULATOR')

@section('content_header')
<h1>NAVIGASI KAPAL AUTO</h1>
@stop

@section('content')
<p>tampilan</p>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
    <h4><b>VALUE : </b></h4>
        <canvas id="idKompasWidget"></canvas>
    </div>
    <div class="col-md-4"></div>
</div>
<div class="row">
    <div class="col-md-3"><canvas id="temp-gauge"></canvas></div>
    <div class="col-md-3">
        <canvas id="temp-gauge"></canvas>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
</div>


@stop

@section('adminlte_js')
<script src="{{ asset('vendor/adminlte/dist/js/app.min.js') }}"></script>
<script src="{{ asset('bower_components/canv-gauge/gauge.js') }}"></script>

@yield('js')
<script>
    var tempGauge = new Gauge({
        renderTo: 'temp-gauge',
        width: 280,
        height: 280,
        glow: true,
        units: 'RPM',
        title: 'KECEPATAN',
        minValue: -10,
        maxValue: 50,
        majorTicks: [ -10, 0, 10, 20, 30, 40, 50 ],
        minorTicks: 10,
        strokeTicks: false,
        highlights: [
        { from: -10, to: 0, color: 'rgba(0, 0, 255, .3)' },
        { from: 0, to: 10, color: 'rgba(0, 0, 255, .1)' },
        { from: 30, to: 35, color: 'rgba(255, 255, 0, .3)' },
        { from: 35, to: 50, color: 'rgba(255, 0, 0, .3)' }
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.onready = function() {
        tempGauge.setValue(20);
    };
    tempGauge.draw();
    $(window).resize(function() {
        if ($(window).width() < 1200) {
            tempGauge.updateConfig({ width: 200, height: 200 });
        } else {
            tempGauge.updateConfig({ width: 280, height: 280 });
        }
    });
</script>

<script>
    window.onload = function () {

        var gauge = new Gauge({
            renderTo: 'idKompasWidget',
            width: 250,
            height: 250,
            glow: false,
            units: false,
            title: false,
            minValue: 0,
            maxValue: 360,
            majorTicks: ['S', 'SW', 'W', 'NW', 'N', 'NE', 'E', 'SE', 'S'],
            minorTicks: 22,
            ticksAngle: 360,
            startAngle: 0,
            strokeTicks: false,
            highlights: false,
            colors: {
                plate: '#222',
                majorTicks: '#f5f5f5',
                minorTicks: '#ddd',
                title: '#fff',
                units: '#ccc',
                numbers: '#eee',
                needle: {
                    start: 'rgba(240, 128, 128, 1)',
                    end: 'rgba(255, 160, 122, .9)',
                    circle: {
                        outerStart: '#ccc',
                        outerEnd: '#ccc',
                        innerStart: '#222',
                        innerEnd: '#222'
                    },
                    shadowUp: false,
                    shadowDown: false
                },
                valueBox: {
                    rectStart: '#888',
                    rectEnd: '#666',
                    background: '#babab2',
                    shadow: 'rgba(0, 0, 0, 1)'
                },
                valueText: {
                    foreground: '#444',
                    shadow: 'rgba(0, 0, 0, 0.3)'
                },
                circle: {
                    shadow: 'rgba(0, 0, 0, 0.5)',
                    outerStart: '#ddd',
                    outerEnd: '#aaa',
                    middleStart: '#eee',
                    middleEnd: '#f0f0f0',
                    innerStart: '#fff',
                    innerEnd: '#fff'
                }
            },
            circles: {
                outerVisible: false,
                middleVisible: false,
                innerVisible: true
            },
            needle: {
                type: 'line',
                end: 89,
                start: 65,
                width: 3,
                circle: {
                    size: 15,
                    inner: false,
                    outer: true
                }
            },
            valueBox: {
                visible: false
            },
            valueText: {
                visible: false
            },
            animation: {
                delay: 10,
                duration: 1000,
                fn: 'linear'
            }
        });

        gauge.onready = function () {
            setInterval(function () {
                gauge.setValue(Math.random() * (195 - 165) + 165);
            }, 1000);
        };

        gauge.setRawValue(180);

        gauge.draw();
    }
    ;
</script>

@stop