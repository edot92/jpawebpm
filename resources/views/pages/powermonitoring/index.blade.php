@extends('adminlte::page')

@section('title', 'PROJECT JPA')

@section('content_header')
<h1>DEVICE MONITORING</h1>
@stop

@section('content')
<p><label for="updated_at" value="eee">Last Updated:</label>
  <input type="text" name="updated_at" id="updated_at" value="" placeholder="" readonly="true"></p>

  <div class="row">
    <div class="col-md-3">

      <div class="row">
        <canvas id="gauge1"></canvas>
      </div>
      <div class="row">
        <canvas id="gauge2"></canvas>
      </div>
      <div class="row">
        <canvas id="gauge3"></canvas>
      </div>
    </div>

    <div class="col-md-8 center-block" >      

     <!--  <h1>Custom Ajax</h1>
     <p>Use <code>ajax</code> option: A method to replace ajax call. Should implement the same API as jQuery ajax method.</p> -->
     <div class="table-responsive">
      <div class="table">
       <div id="tabelRealTimeCSV"></div>
       <table class="table table-bordered table-striped table-hover" style="width: 89%;"">
         <div class="table-responsive">

         </div>
       </div>
     </div>
   </div>

 </div>

 <meta name="_token" content="{!! csrf_token() !!}" />
 @stop

 @section('adminlte_js')
 <script src="{{ asset('vendor/adminlte/dist/js/app.min.js') }}"></script>
 <script src="{{ asset('bower_components/canv-gauge/gauge.js') }}"></script>
 <script src="{{ asset('bower_components/moment/moment.js') }}"></script>
 @yield('js')
 <script>

    // your custom ajax request here
    $(document).ready(function(){
      var tempGauge = new Gauge({
      renderTo: 'gauge1',
      width: 200,
      height: 200,
      glow: true,
      units: 'Volt',
      title: 'Phase 1 Voltage (L-N)',
      minValue: -10,
      maxValue: 50,
      majorTicks: [ -10, 0, 10, 20, 30, 40, 50 ],
      minorTicks: 10,
      strokeTicks: false,
      highlights: [
      { from: -10, to: 0, color: 'rgba(0, 0, 255, .3)' },
      { from: 0, to: 10, color: 'rgba(0, 0, 255, .1)' },
      { from: 30, to: 35, color: 'rgba(255, 255, 0, .3)' },
      { from: 35, to: 50, color: 'rgba(255, 0, 0, .3)' }
      ],
      colors: {
        plate: '#f5f5f5',
        majorTicks: '#666',
        minorTicks: '#888',
        title: '#444',
        units: '#000',
        numbers: '#222',
        needle: {
          start: 'rgba(240, 128, 128, 2)',
          end: 'rgba(255, 160, 122, .9)'
        }
      },
      animation: {
        delay : 25,
        duration: 500,
        fn : 'linear'
      },
      valueFormat: {
        int: 2,
        dec: 1
      }
    });
     tempGauge2 = new Gauge({
      renderTo: 'gauge2',
      width: 200,
      height: 200,
      glow: true,
      units: 'Volt',
      title: 'Phase 2 Voltage (L-N)',
      minValue: -10,
      maxValue: 300,
      majorTicks: [ -10, 0, 10, 20, 30, 40, 50 ],
      minorTicks: 10,
      strokeTicks: false,
      highlights: [
      { from: -10, to: 0, color: 'rgba(0, 0, 255, .3)' },
      { from: 0, to: 10, color: 'rgba(0, 0, 255, .1)' },
      { from: 30, to: 35, color: 'rgba(255, 255, 0, .3)' },
      { from: 35, to: 50, color: 'rgba(255, 0, 0, .3)' }
      ],
      colors: {
        plate: '#f5f5f5',
        majorTicks: '#666',
        minorTicks: '#888',
        title: '#444',
        units: '#000',
        numbers: '#222',
        needle: {
          start: 'rgba(240, 128, 128, 2)',
          end: 'rgba(255, 160, 122, .9)'
        }
      },
      animation: {
        delay : 25,
        duration: 500,
        fn : 'linear'
      },
      valueFormat: {
        int: 2,
        dec: 1
      }
    });

     tempGauge3 = new Gauge({
      renderTo: 'gauge3',
      width: 200,
      height: 200,
      glow: true,
      units: 'Volts',
      title: 'Phase 3 (L-N)',
      minValue: 0,
      maxValue: 300,
      majorTicks: [ 0, 25,50,75,100,125,150,175,200,225,250,275,300 ],
      minorTicks: 10,
      strokeTicks: false,
      highlights: [
      { from: -10, to: 0, color: 'rgba(0, 0, 255, .3)' },
      { from: 0, to: 10, color: 'rgba(0, 0, 255, .1)' },
      { from: 30, to: 35, color: 'rgba(255, 255, 0, .3)' },
      { from: 35, to: 50, color: 'rgba(255, 0, 0, .3)' }
      ],
      colors: {
        plate: '#f5f5f5',
        majorTicks: '#666',
        minorTicks: '#888',
        title: '#444',
        units: '#000',
        numbers: '#222',
        needle: {
          start: 'rgba(240, 128, 128, 2)',
          end: 'rgba(255, 160, 122, .9)'
        }
      },
      animation: {
        delay : 25,
        duration: 500,
        fn : 'linear'
      },
      valueFormat: {
        int: 3,
        dec: 3
      }
    });
    tempGauge.onready = function() {
      tempGauge.setValue(20);
    };
    tempGauge.draw();
    tempGauge2.onready = function() {
      tempGauge2.setValue(20);
    };
    tempGauge2.draw();
    tempGauge3.onready = function() {
      tempGauge3.setValue(20);
    };
    tempGauge3.draw();
    $(window).resize(function() {
      if ($(window).width() < 1200) {
        tempGauge.updateConfig({ width: 100, height: 100 });
      } else {
        tempGauge.updateConfig({ width: 200, height: 200 });
      }
    });

      getDataAjax();
      function getDataAjax(){
        $.get( '/deviceregisterGetValue', function (data) {
            //success data
       //     alert(data[5]['id']);
       var bb="";
       for(x=0;x<data.length;x++){
        bb=bb+data[x]['id']+","+data[x]["register_label_"]+","+data[x]["register_rtu_"]+","+data[x]["display_option_"]+","+data[x]["register_value_"]+"\n";
           if(data[x]["register_label_"]=="Phase 3 Voltage (L-N)")
        {
          var nilaiGauge=parseFloat(data[x]["register_value_"]);
         // tempGauge3.redraw();

          tempGauge3.setValue(nilaiGauge);
        
        };
      }

      var html = '<table id="miyazaki"  class=table table-bordered table-striped table-hover" style="width: 89%;"><thead><tr><th> No <th> Label Registers <th> RTU Register <th> Display Option <th> Value Register<tbody>';
      var waktu = data[0]["updated_at"];
      waktu=moment(waktu).format("hh:mm:ss DD-MM-YYYY");
      document.getElementById("updated_at").value =  waktu;
    // split into lines
    var rows = bb.split("\n");

    // parse lines
    var cnt=0;
    rows.forEach( function getvalues(ourrow) {
        // start a table row
        html += "<tr>";

        // split line into columns
        var columns = ourrow.split(",");
        if(columns[0]!=''){
          html += "<td>" + columns[0] + "</td>";
          html += "<td>" + columns[1] + "</td>";
          html += "<td>" + columns[2] + "</td>";
          html += "<td>" + columns[3] + "</td>";
          html += "<td>" + columns[4] + "</td>";
    //     html += "<td>" + columns[3] + "</td>";
    //     html += "<td>" + columns[4] + "</td>";
    //     html += "<td>" + columns[4] + "</td>";
  }

        // close row
        html += "</tr>";        
      })
    // close table
    html += "</table>";

    // insert into div
    $('#tabelRealTimeCSV').html(html);

    var headertext = [],
    headers = document.querySelectorAll("#miyazaki th"),
    tablerows = document.querySelectorAll("#miyazaki th"),
    tablebody = document.querySelector("#miyazaki tbody");

    for(var i = 0; i < headers.length; i++) {
      var current = headers[i];
      headertext.push(current.textContent.replace(/\r?\n|\r/,""));
    } 
    for (var i = 0, row; row = tablebody.rows[i]; i++) {
      for (var j = 0, col; col = row.cells[j]; j++) {
        col.setAttribute("data-th", headertext[j]);
      } 
    }
  }) ;
        setTimeout(getDataAjax,500);
      }

    });
  </script>
  
<!-- <script>
//rev http://codepen.io/dudleystorey/pen/Geprd

$.get('realValue2.csv', function(data) {

    // start the table  
    // var html = '<table id="miyazaki"  class="table table-bordered"><thead><tr><th>Film<th>Year<th>Honor<tbody>';
    //   <table ">
        var html = '<table id="miyazaki"  class="table table-bordered table-striped table-hover"><thead><tr><th> No <th> Label Registers <th>Phase 1<th> Phase 2 <th>Phase 3<th>Total<tbody>';
   

    // split into lines
    var rows = data.split("\n");

    // parse lines
    var cnt=0;
    rows.forEach( function getvalues(ourrow) {
        // start a table row
        html += "<tr>";

        // split line into columns
        var columns = ourrow.split(",");
     if(columns[0]!=''){
        html += "<td>" + columns[0] + "</td>";
        html += "<td>" + columns[1] + "</td>";
        html += "<td>" + columns[2] + "</td>";
        html += "<td>" + columns[3] + "</td>";
        html += "<td>" + columns[4] + "</td>";
        html += "<td>" + columns[4] + "</td>";
    }
      
        // close row
        html += "</tr>";        
    })
    // close table
    html += "</table>";

    // insert into div
    $('#tabelRealTimeCSV').append(html);

    var headertext = [],
    headers = document.querySelectorAll("#miyazaki th"),
    tablerows = document.querySelectorAll("#miyazaki th"),
    tablebody = document.querySelector("#miyazaki tbody");

    for(var i = 0; i < headers.length; i++) {
      var current = headers[i];
      headertext.push(current.textContent.replace(/\r?\n|\r/,""));
  } 
  for (var i = 0, row; row = tablebody.rows[i]; i++) {
      for (var j = 0, col; col = row.cells[j]; j++) {
        col.setAttribute("data-th", headertext[j]);
    } 
}


});
</script> -->

<!-- <style type="text/css">
    table#miyazaki { 
      margin: 0 auto;
      border-collapse: collapse;
      font-family: Agenda-Light, sans-serif;
      font-weight: 100; 
      background: #333; color: #fff;
      text-rendering: optimizeLegibility;
      border-radius: 5px; 
  }
  table#miyazaki caption { 
      font-size: 2rem; color: #444;
      margin: 1rem;
      /*  background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/miyazaki.png), url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/miyazaki2.png);*/
      background-size: contain;
      background-repeat: no-repeat;
      background-position: center left, center right; 
  }
  table#miyazaki thead th { font-weight: 600; }
  table#miyazaki thead th, table#miyazaki tbody td { 
      padding: .8rem; font-size: 1.4rem;
  }
  table#miyazaki tbody td { 
      padding: .8rem; font-size: 1.4rem;
      color: #444; background: #eee; 
  }
  table#miyazaki tbody tr:not(:last-child) { 
      border-top: 1px solid #ddd;
      border-bottom: 1px solid #ddd;  
  }

  @media screen and (max-width: 600px) {
  table#miyazaki caption { background-image: none; }
  table#miyazaki thead { display: none; }
  table#miyazaki tbody td { 
  display: block; padding: .6rem; 
}
table#miyazaki tbody tr td:first-child { 
background: #666; color: #fff; 
}
table#miyazaki tbody td:before { 
content: attr(data-th); 
font-weight: bold;
display: inline-block;
width: 6rem;  
}
} -->
</style>
@stop