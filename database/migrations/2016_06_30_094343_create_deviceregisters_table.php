<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeviceregistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deviceregisters', function(Blueprint $table) {
            $table->increments('id');
            $table->string('register_label_');
            $table->string('register_rtu_');
            $table->string('register_tcp_');
            $table->string('display_option_');
            $table->string('register_multiply_');
             $table->string('register_value_');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deviceregisters');
    }
}
